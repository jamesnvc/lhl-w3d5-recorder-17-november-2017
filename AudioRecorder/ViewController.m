//
//  ViewController.m
//  AudioRecorder
//
//  Created by James Cash on 17-11-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@import AVFoundation;

@interface ViewController ()

@property (strong,nonatomic) AVAudioRecorder *recorder;
@property (strong,nonatomic) AVAudioPlayer *player;
@property (strong,nonatomic) NSURL *fileURL;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    NSArray *documentsPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    self.fileURL = [NSURL fileURLWithPath:[[documentsPaths firstObject] stringByAppendingPathComponent:@"recorded.mp4"]];
    NSLog(@"Documents directory: %@", documentsPaths);
    NSLog(@"Saving file to %@", self.fileURL);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)toggleRecord:(UIButton *)sender {
    if ([self.recorder isRecording]) {
        [self.recorder stop];
        [sender setTitle:@"Record" forState:UIControlStateNormal];
        return;
    }

    [sender setTitle:@"Stop" forState:UIControlStateNormal];

    NSError *err = nil;
    self.recorder = [[AVAudioRecorder alloc]
                     initWithURL:self.fileURL
                     settings:@{AVNumberOfChannelsKey: @(2),
                                AVSampleRateKey: @(44100),
                                AVFormatIDKey: @(kAudioFormatMPEG4AAC)}
                     error:&err];
    if (err != nil) {
        NSLog(@"Error creating recorder: %@", err.localizedDescription);
        return;
    }
    [self.recorder record];
}

- (IBAction)togglePlay:(UIButton *)sender {
    if ([self.player isPlaying]) {
        [self.player stop];
        [sender setTitle:@"Play" forState:UIControlStateNormal];
        return;
    }
    [sender setTitle:@"Stop" forState:UIControlStateNormal];

    NSError *err = nil;
    self.player = [[AVAudioPlayer alloc]
                   initWithContentsOfURL:self.fileURL
                   error:&err];
    if (err != nil) {
        NSLog(@"Error creating player: %@", err.localizedDescription);
        return;
    }
    [self.player play];
}


@end
